package tugas3;
//author Desti Rizky
public class AlamatOrtu extends Alamat {
    private String namaJln, namaPJ;
    private int noRumah, kodepos;
    
    public AlamatOrtu(String namaJln, int noRumah, int kodepos, String namaPJ){
        super(namaJln);
        this.noRumah=noRumah;
        this.kodepos=kodepos;
        this.namaPJ=namaPJ;
    }

    public String getNamaPJ() {
        return namaPJ;
    }

    public void setNamaPJ(String namaPJ) {
        this.namaPJ = namaPJ;
    }

    public int getNoRumah() {
        return noRumah;
    }

    public void setNoRumah(int noRumah) {
        this.noRumah = noRumah;
    }

    public int getKodepos() {
        return kodepos;
    }

    public void setKodepos(int kodepos) {
        this.kodepos = kodepos;
    }    
}