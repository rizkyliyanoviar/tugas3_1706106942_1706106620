package tugas3;
//author Desti Rizky
public class AlamatKos extends Alamat {
    private String namaPK;
    private int noRumah, noKamar, kodepos;
    
    public AlamatKos(String namaJln, int noRumah, int noKamar, int kodepos, String namaPK){
        super(namaJln);
        this.noRumah=noRumah;
        this.noKamar=noKamar;
        this.kodepos=kodepos;
        this.namaPK=namaPK;
    }

    public String getNamaPK() {
        return namaPK;
    }

    public void setNamaPK(String namaPK) {
        this.namaPK = namaPK;
    }

    public int getNoRumah() {
        return noRumah;
    }

    public void setNoRumah(int noRumah) {
        this.noRumah = noRumah;
    }

    public int getNoKamar() {
        return noKamar;
    }

    public void setNoKamar(int noKamar) {
        this.noKamar = noKamar;
    }

    public int getKodepos() {
        return kodepos;
    }

    public void setKodepos(int kodepos) {
        this.kodepos = kodepos;
    }    
}