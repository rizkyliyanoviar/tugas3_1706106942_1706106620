package tugas3;
//author Desti Rizky
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static tugas3.Tugas3.npm;

public class DB {
    /*Hasmap terdiri dari int npm sebagai key dan DataMhs*/
    private static HashMap<Integer, DataMhs> DB = new HashMap<>();
    public static String opening, dataMhs, dataOrtu, dataKos, dataAsrama, closing;
    
    /*Method untuk menuliskan data ke file .txt*/
    static void saveDataToFile(String file) {
        
        StringBuilder sb = new StringBuilder();
            opening = "DATA MAHASISWA\n\n\n";
        for (Map.Entry temp : DB.entrySet()) {
            dataMhs = "Nama Mahasiswa: " + DB.get(npm).getMhs().getNama()+
                    "\nNPM: " + DB.get(npm).getMhs().getNpm()+
                    "\nUmur: " + DB.get(npm).getMhs().getUmur()+
                    "\n--------------------------------------------"+
                    "\nAlamat Orang Tua: "+
                    "\n--------------------------------------------";
            for (int i = 0; i < DB.get(temp.getKey()).getAlamat().size(); i++) {
                if (DB.get(temp.getKey()).getAlamat().get(i) instanceof AlamatOrtu) {
                    
                    dataOrtu = "\nNama Jalan: " + (DB.get(temp.getKey()).getAlamat().get(i)).getNamaJln()+
                    "\nNomor Rumah: " + ((AlamatOrtu) DB.get(temp.getKey()).getAlamat().get(i)).getNoRumah()+
                    "\nKode Pos: " + ((AlamatOrtu) DB.get(temp.getKey()).getAlamat().get(i)).getKodepos()+
                    "\nNama Penanggungjawab: " + ((AlamatOrtu) DB.get(temp.getKey()).getAlamat().get(i)).getNamaPJ()+
                    "\n--------------------------------------------";
                } else if (DB.get(temp.getKey()).getAlamat().get(i) instanceof AlamatKos) {
                    dataKos = "\nAlamat Kos: "+
                            "\n--------------------------------------------"+
                            "\nNama Jalan: " + (DB.get(temp.getKey()).getAlamat().get(i)).getNamaJln()+
                    "\nNomor Rumah: " + ((AlamatKos) DB.get(temp.getKey()).getAlamat().get(i)).getNoRumah()+
                    "\nNomor Kamar: " + ((AlamatKos) DB.get(temp.getKey()).getAlamat().get(i)).getNoKamar()+
                    "\nKode Pos: " + ((AlamatKos) DB.get(temp.getKey()).getAlamat().get(i)).getKodepos()+
                    "\nNama Pemilik Kos: " + ((AlamatKos) DB.get(temp.getKey()).getAlamat().get(i)).getNamaPK()+
                            "\n--------------------------------------------";
                } else {
                    dataAsrama = "\nAlamat Asrama: "+
                            "\n--------------------------------------------"+
                            "\nNama Jalan: " + (DB.get(temp.getKey()).getAlamat().get(i)).getNamaJln()+
                    "\nNama Asrama: " + ((AlamatAsrama) DB.get(temp.getKey()).getAlamat().get(i)).getNamaAsrama()+
                    "\nNama Jalan: " + ((AlamatAsrama) DB.get(temp.getKey()).getAlamat().get(i)).getNamaJalan()+
                    "\nNomor Kamar: " + ((AlamatAsrama) DB.get(temp.getKey()).getAlamat().get(i)).getNomorKamar()+
                    "\nMasa Tinggal: " + ((AlamatAsrama) DB.get(temp.getKey()).getAlamat().get(i)).getMasaTinggal();
                }
            }
            closing = "\n\n\nPT. Mencari Cinta Sejati (di Fasilkom?)";
        }
        sb.append(opening+dataMhs+dataOrtu+dataKos+dataAsrama+closing);
        try (PrintStream out = new PrintStream(new FileOutputStream(file))) {
            out.print(sb.toString());
        }

    catch(IOException e)
    { e.printStackTrace();
    }
    }


public DB(HashMap<Integer, DataMhs> DB) {
        this.DB = DB;
    }

    public DB() {
    }
    
    /*Method untuk menambahkan data mahasiswa*/
    public static void addDataMhs(Integer npm, DataMhs data) {
        DB.put(npm, data);
    }
    
    /*Method untuk menghapus data mahasiswa*/
    public static void deleteDataMhs(Integer npm) {
        DB.remove(npm);
    }
    
    /*Method untul menampilkan data mahasiswa berdasarkan npm*/
    public static void showDataMhsByNPM(Integer npm) {
        System.out.println("Nama Mahasiswa: " + DB.get(npm).getMhs().getNama());
        System.out.println("NPM: " + DB.get(npm).getMhs().getNama());
        System.out.println("Umur: " + DB.get(npm).getMhs().getUmur());
        for (int i = 0; i < DB.get(npm).getAlamat().size(); i++) {
            if (DB.get(npm).getAlamat().get(i) instanceof AlamatOrtu) {
                System.out.println("ini kelas : " + DB.get(npm).getAlamat().get(i).getClass().getSimpleName());
                System.out.println(((AlamatOrtu) DB.get(npm).getAlamat().get(i)).getNamaPJ());
            } else if (DB.get(npm).getAlamat().get(i) instanceof AlamatKos) {
                System.out.println("ini kelas : " + DB.get(npm).getAlamat().get(i).getClass().getSimpleName());
                System.out.println(((AlamatKos) DB.get(npm).getAlamat().get(i)).getNamaPK());
            } else {
                System.out.println("ini kelas : " + DB.get(npm).getAlamat().get(i).getClass().getSimpleName());
                System.out.println(((AlamatAsrama) DB.get(npm).getAlamat().get(i)).getNamaAsrama());
            }
        }
        System.out.println();
    }
    
    /*Method untuk menampilkan seluruh data mahasiswa yang tersimpan*/
    public static void showAllDataMhs() {
       System.out.println("Jumlah Mahasiswa : " + DB.size());
        for (Map.Entry temp : DB.entrySet()) {
            System.out.println("Nama Mahasiswa\t: " + DB.get(temp.getKey()).getMhs().getNama());
            System.out.println("NPM\t\t: " + DB.get(temp.getKey()).getMhs().getNpm());
            System.out.println("Umur Mahasiswa\t: " + DB.get(temp.getKey()).getMhs().getUmur());
            for (int i = 0; i < DB.get(temp.getKey()).getAlamat().size(); i++) {
                if (i == 0) {
                    System.out.println("\n--------------------------------------------");
                    System.out.println("Alamat Orang Tua: ");
                    System.out.println("\n--------------------------------------------");
                    System.out.println("Nama Jalan: " + (DB.get(temp.getKey()).getAlamat().get(0)).getNamaJln());
                    System.out.println("Nomor Rumah: " + ((AlamatOrtu) DB.get(temp.getKey()).getAlamat().get(0)).getNoRumah());
                    System.out.println("Kode Pos: " + ((AlamatOrtu) DB.get(temp.getKey()).getAlamat().get(0)).getKodepos());
                    System.out.println("Nama Penanggungjawab: " + ((AlamatOrtu) DB.get(temp.getKey()).getAlamat().get(0)).getNamaPJ());
                } else if (i == 1) {
                    System.out.println("\n--------------------------------------------");
                    System.out.println("Alamat Kost: ");
                    System.out.println("\n--------------------------------------------");
                    System.out.println("Nama Jalan: " + (DB.get(temp.getKey()).getAlamat().get(1)).getNamaJln());
                    System.out.println("Nomor Rumah: " + ((AlamatKos) DB.get(temp.getKey()).getAlamat().get(1)).getNoRumah());
                    System.out.println("Nomor Kamar: " + ((AlamatKos) DB.get(temp.getKey()).getAlamat().get(1)).getNoKamar());
                    System.out.println("Kode Pos: " + ((AlamatKos) DB.get(temp.getKey()).getAlamat().get(1)).getKodepos());
                    System.out.println("Nama Pemilik Kos: " + ((AlamatKos) DB.get(temp.getKey()).getAlamat().get(1)).getNamaPK());
                } else if (i == 2) {
                    System.out.println("\n--------------------------------------------");
                    System.out.println("Alamat Asrama: ");
                    System.out.println("\n--------------------------------------------");
                    System.out.println("Nama Jalan: " + (DB.get(temp.getKey()).getAlamat().get(2)).getNamaJln());
                    System.out.println("Nama Asrama: " + ((AlamatAsrama) DB.get(temp.getKey()).getAlamat().get(2)).getNamaAsrama());
                    System.out.println("Nomor Jalan: " + ((AlamatAsrama) DB.get(temp.getKey()).getAlamat().get(2)).getNomorJalan());
                    System.out.println("Nomor Kamar: " + ((AlamatAsrama) DB.get(temp.getKey()).getAlamat().get(2)).getNomorKamar());
                    System.out.println("Masa Tinggal: " + ((AlamatAsrama) DB.get(temp.getKey()).getAlamat().get(2)).getMasaTinggal());
                }
            }
        }
    }
}
