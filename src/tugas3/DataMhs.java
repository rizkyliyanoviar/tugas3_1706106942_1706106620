package tugas3;
//author Desti Rizky
import java.util.ArrayList;

public class DataMhs {
    private Mahasiswa mhs;
    private ArrayList<Alamat> alamat;

    public DataMhs(Mahasiswa mhs, ArrayList<Alamat> alamat) {
        this.mhs = mhs;
        this.alamat = alamat;
    }

    public Mahasiswa getMhs() {
        return mhs;
    }

    public void setMhs(Mahasiswa mhs) {
        this.mhs = mhs;
    }

    public ArrayList<Alamat> getAlamat() {
        return alamat;
    }

    public void setAlamat(ArrayList<Alamat> alamat) {
        this.alamat = alamat;
    }
}