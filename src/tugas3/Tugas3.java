package tugas3;
//author Desti Rizky
//import package API
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Tugas3 {

    public Tugas3() {
    }

    public static ArrayList<Alamat> arrayAlamat = new ArrayList<>();            // Arraylist alamat terdiri dari alamat ortu, kos, asrama
    Scanner scanner = new Scanner(System.in);
    public static boolean pilihan = false;
    public static int ops;
    public static int npm;
    public static int jumMhs=0;
    
    //method untuk menambahkan alamat ortu
    void AlamatOrtu() {
        System.out.print("Masukan nama jalan: ");
        String namaJlnOrTu = scanner.next();
        System.out.print("Masukan nomor rumah: ");
        int noRumahOrTu = scanner.nextInt();
        System.out.print("Masukan Kode pos: ");
        int kodeposOrTu = scanner.nextInt();
        System.out.print("Masukan nama penanggungjawab: ");
        String namaPJOrTu = scanner.next();
        AlamatOrtu alamat1 = new AlamatOrtu(namaJlnOrTu, noRumahOrTu, kodeposOrTu, namaPJOrTu);
        arrayAlamat.add(alamat1);
        System.out.print("Isi alamat kost? [Y/N] ");
        String ops2 = scanner.next();
        ops = 2;
        this.opsi(ops2, ops);
    }
    
    //method untuk menambahkan alamat kos
    void AlamatKos() {
        System.out.print("Masukan nama jalan: ");
        String namaJln = scanner.next();
        System.out.print("Masukan nomor Rumah: ");
        int noRumah = scanner.nextInt();
        System.out.print("Masukan nomor Kamar: ");
        int noKamar = scanner.nextInt();
        System.out.print("Masukan kode pos: ");
        int kodePos = scanner.nextInt();
        System.out.print("Masukan nama pemilik kos: ");
        String namaPK = scanner.next();
        AlamatKos alamat2 = new AlamatKos(namaJln, noRumah, noKamar, kodePos, namaPK);
        arrayAlamat.add(alamat2);
        System.out.print("Isi alamat asrama? [Y/N] ");
        String ops3 = scanner.next();
        ops = 3;
        this.opsi(ops3, ops);
    }
    
    //method untuk menambahkan alamat asrama
    void AlamatAsrama() {
        System.out.print("Masukan nama jalan: ");
        String namaJalan = scanner.next();
        System.out.print("Masukan nomor jalan: ");
        int nomorJalan = scanner.nextInt();
        System.out.print("Masukan nama asrama: ");
        String namaAsrama = scanner.next();
        System.out.print("Masukan nomor kamar: ");
        int nomorKamar = scanner.nextInt();
        System.out.print("Masukan masa tinggal di asrama (satuan bulan): ");
        int masaTinggal = scanner.nextInt();
        AlamatAsrama alamat3 = new AlamatAsrama(namaJalan, nomorJalan, namaAsrama, nomorKamar, masaTinggal);
        arrayAlamat.add(alamat3);
        pilihan = false;
    }
    
    /*Method ini adalah untuk mengisi alamat mahasiswa yang terdiri dari 
    alamat ortu, alamat kos, dan alamat asrama. 
    Opsi=1 untuk alamat ortu
    Opsi=2 untuk alamat kost
    Opsi=3 untuk alamat asrama*/
    void opsi(String opsi, int ops) {
        Tugas3 objj = new Tugas3();                                             
        if (ops == 1) {                                                         
            if (opsi.equals("Y") || opsi.equals("y")){
                this.AlamatOrtu();
            }else if (opsi.equals("N") || opsi.equals("n")){
                System.out.print("Isi alamat kost? [Y/N] ");
                String opst = scanner.next();
                ops = 2;
                this.opsi(opst, ops);
            }else{
                System.out.print("Pilihan hanya Y/N!!!\nSilahkan masukkan pilihan lagi [Y/N] ");
                String opst = scanner.next();
                ops = 1;
                this.opsi(opst, ops);
            }
        } else if (ops == 2) {
            if (opsi.equals("Y") || opsi.equals("y")){
                this.AlamatKos();
            }else if (opsi.equals("N") || opsi.equals("n")){
                System.out.print("Isi alamat asrama? [Y/N] ");
                String opst = scanner.next();
                ops = 3;
                this.opsi(opst, ops);
            }else{
                System.out.print("Pilihan hanya Y/N!!!\nSilahkan masukkan pilihan lagi [Y/N] ");
                String opst = scanner.next();
                ops = 2;
                this.opsi(opst, ops);
            }
        } else if (ops == 3) {
            if (opsi.equals("Y") || opsi.equals("y")){
                this.AlamatAsrama();
            }else if (opsi.equals("N") || opsi.equals("n")){
                
            }else{
                System.out.print("Pilihan hanya Y/N!!!\nSilahkan masukkan pilihan lagi [Y/N] ");
                String opst = scanner.next();
                ops = 3;
                this.opsi(opst, ops);
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Tugas3 obj = new Tugas3();
        Scanner scanner = new Scanner(System.in);
        
        /*Dilakukan perulangan selama nilai pilihan bernilai false. 
        Nilai pilihan akan berubah menjadi true. Itu terjadi saat user memasukan
        pilihan 6*/
        while (pilihan == false) {
            System.out.println("PILIHAN ANDA:");
            System.out.println("1. Tambahkan data mahasiswa");
            System.out.println("2. Hapus data mahasiswa");
            System.out.println("3. Cetak semua data ke layar");
            System.out.println("4. Ambil data dari file");
            System.out.println("5. Simpan data ke file");
            System.out.println("6. Selesai");

            int input = scanner.nextInt();                                      // input pilihan pengguna
            
            /*Jika input pilihan 1, maka jumlah mahasiswa akan bertambah 1.
            Disini akan dibuat objek mahasiswa baru dengan data yang diiputkan.
            */
            if (input == 1) {
                jumMhs+=1;
                System.out.print("Masukan Nama: ");
                String namaMhs = scanner.next();
                System.out.print("Masukan NPM: ");
                npm = scanner.nextInt();
                System.out.print("Masukan Umur: ");
                int umur = scanner.nextInt();
                System.out.print("Isi alamat orangtua? [Y/N] ");
                String ops1 = scanner.next();
                Mahasiswa mhs = new Mahasiswa(namaMhs, npm, umur);
                ops = 1;                
                DataMhs data = new DataMhs(mhs, arrayAlamat);
                obj.opsi(ops1, ops);
                
                DB.addDataMhs(npm, data);
            }
            
            /*Jika input pilihan 2, maka jumlah mahasiswa akan berkurang 1.
            Disini akan dihapus salah satu objek mahasiswa sesuai dengan npm.
            Ini dilakukan dengan menggunakan method deleteDataMhs dari class DB*/
            if (input == 2) {
                System.out.print("Masukkan NPM Mahasiswa yang akan dihapus : ");
                int npmHapus = scanner.nextInt();
                DB.deleteDataMhs(npmHapus);
                jumMhs-=1;
                System.out.println("Data berhasil dihapus!\n");
                System.out.println("====================");
            }
            
            /*Jika input pilihan 3, maka akan ditampilkan seluruh data mahasiswa
            yang ada. Ini dilakukan dengan menggunakan showAllDataMhs dari class DB*/
            if (input == 3) {
                System.out.println("====================");
                DB.showAllDataMhs();
            }
            
            /*Jika input pilihan 4, maka akan diambil data dari file txt yang tersedia*/
            if (input == 4) {
                //List<String> words = new ArrayList<String>();
//                BufferedReader reader = new BufferedReader(new FileReader("D:/Universitas Indonesia/Semester 3/Tugas3/dataMhs.txt"));
//                String line;
//                while ((line = reader.readLine()) != null) {
//                    arrayAlamat.add(line);
//                }
//                reader.close();
            }
            
            /*Jika input pilihan 5, maka data yang tersimpan dalam data 
            mahaiswa akan tersimpan dalam bentuk .txt*/
            if (input == 5) {
                String file = "dataMahasiswa.txt";
                DB.saveDataToFile(file);
            }
            
            /*Jika input pilihan 6, nilai pilihan akan berubah menjadi true,
            sehingga perulangan akan berhenti dan keluar dari program*/
            if (input == 6) {
                System.out.print("Anda telah keluar dari program!");
                pilihan = true;
            }
        }
    }
}
