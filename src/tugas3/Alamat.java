package tugas3;
//author Desti Rizky
public class Alamat {
    String namaJln;
    
    public Alamat(String namaJln){
        this.namaJln=namaJln;
    }

    public String getNamaJln() {
        return namaJln;
    }

    public void setNamaJln(String namaJln) {
        this.namaJln = namaJln;
    }    
}