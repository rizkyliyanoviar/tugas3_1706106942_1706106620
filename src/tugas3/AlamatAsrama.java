package tugas3;
//author Desti Rizky
public class AlamatAsrama extends Alamat {
    private String namaJalan, namaAsrama;
    private int nomorJalan, nomorKamar, masaTinggal;

    public AlamatAsrama(String namaJalan, int nomorJalan, String namaAsrama, int nomorKamar, int masaTinggal) {
        super(namaJalan);
        this.namaAsrama = namaAsrama;
        this.nomorJalan = nomorJalan;
        this.nomorKamar = nomorKamar;
        this.masaTinggal = masaTinggal;
    }

    public String getNamaJalan() {
        return namaJalan;
    }

    public void setNamaJalan(String namaJalan) {
        this.namaJalan = namaJalan;
    }

    public String getNamaAsrama() {
        return namaAsrama;
    }

    public void setNamaAsrama(String namaAsrama) {
        this.namaAsrama = namaAsrama;
    }

    public int getNomorJalan() {
        return nomorJalan;
    }

    public void setNomorJalan(int nomorJalan) {
        this.nomorJalan = nomorJalan;
    }

    public int getNomorKamar() {
        return nomorKamar;
    }

    public void setNomorKamar(int nomorKamar) {
        this.nomorKamar = nomorKamar;
    }

    public int getMasaTinggal() {
        return masaTinggal;
    }

    public void setMasaTinggal(int masaTinggal) {
        this.masaTinggal = masaTinggal;
    }
}